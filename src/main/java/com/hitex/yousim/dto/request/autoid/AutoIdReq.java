package com.hitex.yousim.dto.request.autoid;

import com.hitex.yousim.model.AutoId;
import lombok.Data;

@Data
public class AutoIdReq extends AutoId {
    private String textSearch;


}