package com.hitex.yousim.dto.response.hoadon;

import com.hitex.yousim.model.HoaDon;
import lombok.Data;

import java.util.List;

@Data
public class HoaDonRes  {
    List<HoaDon> listHoaDon;
}