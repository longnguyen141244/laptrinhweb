package com.hitex.yousim.dto.response.autoid;

import com.hitex.yousim.model.AutoId;
import lombok.Data;

@Data
public class AutoIdResponse  {
    AutoId autoId;
}
