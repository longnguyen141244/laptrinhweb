package com.hitex.yousim.dto.response.autoid;

import com.hitex.yousim.model.AutoId;
import lombok.Data;

import java.util.List;

@Data
public class AutoIdRes  {
    List<AutoId> listAutoId;
}