package com.hitex.yousim.dto.response.area;

import com.hitex.yousim.model.Area;
import lombok.Data;

import java.util.List;

@Data
public class AreaRes  {
    List<Area> listArea;
}