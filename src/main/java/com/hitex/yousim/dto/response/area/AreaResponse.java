package com.hitex.yousim.dto.response.area;

import com.hitex.yousim.model.Area;
import lombok.Data;

@Data
public class AreaResponse  {
    Area area;
}
