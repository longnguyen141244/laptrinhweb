package com.hitex.yousim.dto.response.khachhang;

import com.hitex.yousim.model.KhachHang;
import lombok.Data;

@Data
public class KhachHangResponse {
    KhachHang KhachHang;
}
